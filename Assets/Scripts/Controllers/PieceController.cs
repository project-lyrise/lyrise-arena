﻿using Assets.Scripts.Data;
using Assets.Scripts.Enums;
using Assets.Scripts.Helpers;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class PieceController : MonoBehaviour, IMovable
    {
        public Creature Creature { get; private set; }

        [HideInInspector]
        public IShape CurrentField { get; private set; }

        public PlayerType PlayerType { get; set; }

        [HideInInspector]
        public int CurrentActionPoints;

        private bool _isHoverStateChanged;

        private HexGridController HexGrid => GetComponentInParent<HexGridController>();

        // Start is called before the first frame update
        void Start()
        {
        }

        public void InitializeCreatureData(Creature pieceCreature, IShape startField, PlayerType piecePlayer)
        {
            this.Creature = pieceCreature;
            CurrentField = startField;
            this.transform.position = CurrentField.Position;
            CurrentActionPoints = Creature.ActionPoints;
            SetPieceSprite(Creature.GetDominatingAspect());
            this.PlayerType = piecePlayer;
        }

        void Update()
        {
            #region Hover
            Vector3 mousePosition = MouseHelper.GetMouseWorldPoint(Camera.main);
            Vector3 roundedHexPosition = HexHelper.WorldPositionToAxial(mousePosition, CurrentField.Radius);

            if (!_isHoverStateChanged && roundedHexPosition.x == CurrentField.Column && roundedHexPosition.y == CurrentField.Row)
            {
                SetHover(true);
                _isHoverStateChanged = true;
            }
            else if (_isHoverStateChanged && (roundedHexPosition.x != CurrentField.Column || roundedHexPosition.y != CurrentField.Row))
            {
                SetHover(false);
                _isHoverStateChanged = false;
            }
            #endregion
        }

        #region SetPieceSprite()
        private void SetPieceSprite(Aspect aspect)
        {
            switch (aspect)
            {
                case Aspect.Earth:
                    SetSpriteByName("earth_piece");
                    break;

                case Aspect.Fire:
                    SetSpriteByName("fire_piece");
                    break;

                case Aspect.Life:
                    SetSpriteByName("life_piece");
                    break;

                case Aspect.Light:
                    SetSpriteByName("light_piece");
                    break;

                case Aspect.Shadow:
                    SetSpriteByName("shadow_piece");
                    break;

                case Aspect.Void:
                    SetSpriteByName("void_piece");
                    break;

                case Aspect.Water:
                    SetSpriteByName("water_piece");
                    break;

                case Aspect.Wind:
                    SetSpriteByName("wind_piece");
                    break;
            }
        }

        private void SetSpriteByName(string v)
        {
            this.transform.Find(v).gameObject.SetActive(true);
        }
        #endregion

        #region Move()
        /// <summary>
        /// Moves piece to passed destination shape. When isUndoing is true, APs are added instead of subtracted.
        /// </summary>
        /// <param name="destination">Destination field</param>
        /// <param name="isUndoing">Whether the action is being done or undone</param>
        public void Move(IShape destination, bool isUndoing = false)
        {
            Hex currentHex = (Hex)CurrentField;
            Hex destinationHex = (Hex)destination;

            int distance = HexHelper.AxialDistance(currentHex.AxialPosition, destinationHex.AxialPosition);
            CurrentField = destination;
            this.transform.position = destination.Position;

            if (!isUndoing)
            {
                Creature.ActionPoints -= distance;
            }
            else
            {
                Creature.ActionPoints += distance;
            }
        }
        #endregion

        #region SetHover()
        /// <summary>
        /// Set or remove hex hover state
        /// </summary>
        /// <param name="hover"></param>
        public void SetHover(bool hover)
        {
            if (hover)
            {
                GuiManager.PieceStatsPanel.ShowCreatureInfo(Creature);

            }
            else if (!hover && !HexGrid.IsPieceSelected)
            {
                GuiManager.PieceStatsPanel.HideCreatureInfo();
            }
            else
            {
                HexGrid.LoadSelectedPieceDataToGui();
            }
        }
        #endregion
    }
}
