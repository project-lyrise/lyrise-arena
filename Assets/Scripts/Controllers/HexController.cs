﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Data;
using Assets.Scripts.Enums;
using Assets.Scripts.Helpers;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class HexController : MonoBehaviour
    {
        [HideInInspector]
        public Hex HexObject { get; set; }
        private Stack<HexSprite> _spriteStack;

        private bool _isHoverStateChanged;

        // Start is called before the first frame update
        void Start()
        {
            _spriteStack = new Stack<HexSprite>();
            PushSprite(HexSprite.Default);
        }

        // Update is called once per frame
        void Update()
        {
            #region Hover
            Vector3 mousePosition = MouseHelper.GetMouseWorldPoint(Camera.main);
            Vector3 roundedHexPosition = HexHelper.WorldPositionToAxial(mousePosition, HexObject.Radius);

            if(!_isHoverStateChanged && roundedHexPosition == HexObject.AxialPosition)
            {
                SetSprite(HexSprite.Hover);
                _isHoverStateChanged = true;
            }
            else if(_isHoverStateChanged && (roundedHexPosition != HexObject.AxialPosition))
            {
                RemoveCurrentSprite();
                _isHoverStateChanged = false;
            }
            #endregion
        }

        private void OnMouseDown()
        {
            if (Input.GetMouseButton(0))
            {
                gameObject.GetComponentInParent<HexGridController>().OnFieldClick(HexObject);
            }
        }

        #region SetSprite()
        public void SetSprite(HexSprite sprite)
        {
            PushSprite(sprite);
        }
        #endregion

        #region RemoveCurrentSprite()
        public void RemoveCurrentSprite()
        {
            PopSprite();
        }
        #endregion

        #region SpriteStack
        private void PushSprite(HexSprite sprite)
        {
            if(_spriteStack.Any())
            {
                SetHexSprite(_spriteStack.Peek(), false);
            }

            _spriteStack.Push(sprite);
            SetHexSprite(sprite, true);
        }

        private HexSprite PopSprite()
        {
            HexSprite removedObject = HexSprite.Default;

            if(_spriteStack.Count > 1)
            {
                removedObject = _spriteStack.Pop();
                SetHexSprite(removedObject, false);
                SetHexSprite(_spriteStack.Peek(), true);
            }
            else if(_spriteStack.Count == 1)
            {
                removedObject = _spriteStack.Pop();
                SetHexSprite(removedObject, false);
            }

            return removedObject;
        }
        #endregion

        #region SetHexSprite
        private void SetHexSprite(HexSprite sprite, bool isActive)
        {
            switch (sprite)
            {
                case HexSprite.Default:
                    SetSpriteByName("hex", isActive);
                    break;

                case HexSprite.Hover:
                    SetSpriteByName("hex_hover", isActive);
                    break;

                case HexSprite.Movable:
                    SetSpriteByName("hex_movable", isActive);
                    break;

                case HexSprite.Ally:
                    SetSpriteByName("hex_ally", isActive);
                    break;

                case HexSprite.Enemy:
                    SetSpriteByName("hex_enemy", isActive);
                    break;

                default:
                    SetSpriteByName("hex", isActive);
                    break;
            }
        }

        private void SetSpriteByName(string v, bool isActive)
        {
            transform.Find(v).gameObject.SetActive(isActive);
        }
        #endregion
    }
}
