﻿using Assets.Scripts.Interfaces;

namespace Assets.Scripts.Controllers.Commands
{
    public class MoveCommand : ICommand
    {
        private readonly IMovable Target;
        private readonly IShape DestinationField;

        private IShape PreviousField;

        public MoveCommand(IMovable target, IShape destination)
        { 
            this.Target = target;
            this.DestinationField = destination;
        }

        /// <summary>
        /// Executes command.
        /// </summary>
        public void Execute()
        {
            PreviousField = Target.CurrentField;
            Target.Move(DestinationField);
        }

        /// <summary>
        /// Undoes command.
        /// </summary>
        public void Undo()
        {
            Target.Move(PreviousField, true);
        }
    }
}
