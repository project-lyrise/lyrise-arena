﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Controllers.Commands;
using Assets.Scripts.Data;
using Assets.Scripts.Enums;
using Assets.Scripts.Helpers;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class HexGridController : MonoBehaviour, IGrid
    {
        public int GridRadius;
        public float HexRadius;

        [HideInInspector]
        public List<IShape> FieldList { get; set; }

        private List<Hex> _highlightedFields;
        private PieceController _selectedPiece;

        [HideInInspector]
        public bool IsPieceSelected = false;

        private CommandManager _commandManager;

        // Start is called before the first frame update
        void Start()
        {
            _commandManager = new CommandManager();

            _highlightedFields = new List<Hex>();
            GenerateGrid();
        }

        #region GenerateGrid()
        /// <summary>
        /// Generates hex grid based on public gridRadius and public hexRadius
        /// </summary>
        public void GenerateGrid()
        {
            FieldList = new List<IShape>();

            for (int q = -GridRadius; q <= GridRadius; q++)
            {
                int r1 = Mathf.Max(-GridRadius, -q - GridRadius);
                int r2 = Mathf.Min(GridRadius, -q + GridRadius);

                for (int r = r1; r <= r2; r++)
                {
                    GenerateHex(q, r, HexRadius);
                }
            }
        }
        #endregion

        #region GenerateHex()
        /// <summary>
        /// Generates Hex object, instantiates Hex Prefab based on Hex object and adds it to global FieldList.
        /// </summary>
        /// <param name="column">Column in grid</param>
        /// <param name="row">Row in grid</param>
        /// <param name="hexRadius">Radius of hex</param>
        /// <param name="hexPrefab">Prefab containing hex sprite and controller</param>
        private void GenerateHex(
            int column,
            int row,
            float hexRadius,
            GameObject hexPrefab = null)
        {
            try
            {
                //instantiate hex and add to hexTable
                Hex hex = new Hex(
                    column,
                    row,
                    hexRadius
                );

                var hexObject = Instantiate(
                    hexPrefab ?? PrefabManager.HexPrefab,
                    hex.Position,
                    Quaternion.identity
                );

                //set new hex object as a child
                hexObject.transform.parent = gameObject.transform;

                //pass Hex object to HexController
                hexObject.GetComponent<HexController>().HexObject = hex;

                //scale hexObject
                float scale = hexRadius / 0.5f;
                hexObject.transform.localScale = new Vector3(
                    hexObject.transform.localScale.x * scale,
                    hexObject.transform.localScale.y * scale,
                    hexObject.transform.localScale.z * scale);

                //assign gameObject to Hex object
                hex.hexObject = hexObject;

                FieldList.Add(hex);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }
        #endregion

        #region DestroyHex()
        /// <summary>
        /// Removes passed Hex object with hex prefab connected to it.
        /// </summary>
        /// <param name="hex">Hex object to be destroyed</param>
        private void DestroyHex(Hex hex)
        {
            GameObject hexObject = hex.hexObject;
            Destroy(hexObject);
            FieldList.Remove(hex);
        }
        #endregion

        #region OnFieldClick()
        /// <summary>
        /// Executes certain action when clicked on passed field (select, deselect, move) based on current situation.
        /// </summary>
        /// <param name="field">Clicked field</param>
        public void OnFieldClick(Hex field)
        {
            var (isPieceOnField, piece) = IsPieceOnField(field);

            if (isPieceOnField)
            {
                SelectPiece(piece);
            }
            else if (IsPieceSelected)
            {
                if (CheckIfFieldInRange(field))
                {
                    MovePieceToField(_selectedPiece, field);
                    DeselectPiece();
                }
                else
                {
                    DeselectPiece();
                }
            }
        }
        #endregion

        #region MovePieceToField()
        /// <summary>
        /// Executes move command resulting in moving passed piece to passed field.
        /// </summary>
        /// <param name="selectedPiece">Piece to move</param>
        /// <param name="field">Destination field for piece</param>
        private void MovePieceToField(global::Assets.Scripts.Controllers.PieceController selectedPiece, Hex field)
        {
            MoveCommand move = new MoveCommand(selectedPiece, field);
            _commandManager.ExecuteCommand(move);
        }
        #endregion

        #region CheckIfFieldInRange()
        /// <summary>
        /// Checks if passed field is in currently highlited fields.
        /// </summary>
        /// <param name="field">Field to check</param>
        /// <returns></returns>
        private bool CheckIfFieldInRange(Hex field)
        {
            return _highlightedFields.Any(h => h.AxialPosition == field.AxialPosition);
        }
        #endregion

        #region SelectPiece()
        /// <summary>
        /// Selects current piece and shows its reach based on its parameters.
        /// </summary>
        /// <param name="piece">Piece to select</param>
        private void SelectPiece(global::Assets.Scripts.Controllers.PieceController piece)
        {
            if (piece.PlayerType != GameManager.Turn.CurrentPlayer) return;

            _selectedPiece = piece;
            IsPieceSelected = true;

            HideReach();
            ShowReach(piece, piece.CurrentField);
            ShowPieceInfo(piece);
        }

        #endregion

        #region DelesectPiece()
        /// <summary>
        /// Deselects currently selected piece. If no piece is selected, does nothing.
        /// </summary>
        private void DeselectPiece()
        {
            _selectedPiece = null;
            IsPieceSelected = false;

            HideReach();
            HidePieceInfo();
        }
        #endregion

        #region IsPieceOnField()
        /// <summary>
        /// Checks whether there is any object with PieceController associated with passed field.
        /// </summary>
        /// <param name="field">Field to check</param>
        /// <returns>IsPieceOnField: bool containing info whether there is piece on current field
        ///          Piece: PieceController object returned when IsPieceOnField is true, containing data about piece on current field</returns>
        public (bool IsPieceOnField, global::Assets.Scripts.Controllers.PieceController Piece) IsPieceOnField(Hex field)
        {
            var piecesOnBoard = gameObject.GetComponentsInChildren<PieceController>().ToList();
            var hexList = FieldList.ConvertAll(h => (Hex)h);

            foreach (var piece in piecesOnBoard)
            {
                if (piece.CurrentField.Position == field.Position)
                {
                    return (true, piece);
                }
            }

            return (false, null);
        }
        #endregion

        #region ShowReach()

        /// <summary>
        /// Changes sprite of all hexes within piece reach, inserting them into HighlitedFields list.
        /// </summary>
        /// <param name="piece">Piece that contains reach info</param>
        /// <param name="clickedHex">Hex object that is a center for reach calculations</param>
        public void ShowReach(PieceController piece, IShape clickedHex)
        {
            var range = piece.Creature.ActionPoints;
            var hexesToHighlight = new List<Hex>();

            var reachableHexes = HexHelper.GetRange((Hex) piece.CurrentField, FieldList.Cast<Hex>().ToList(), range);

            foreach (var hex in reachableHexes)
            {
                var (isPieceOnField, pieceOnField) = IsPieceOnField(hex);
                var hexController = hex.hexObject.GetComponent<HexController>();

                if (!isPieceOnField)
                {
                    hexController.SetSprite(HexSprite.Movable);
                }
                else
                {
                    if (piece.PlayerType != pieceOnField.PlayerType)
                    {
                        hexController.SetSprite(HexSprite.Enemy);
                    }
                    else
                    {
                        hexController.SetSprite(HexSprite.Ally);
                    }
                }
                hexesToHighlight.Add(hex);
            }

            _highlightedFields = hexesToHighlight;
        }
        #endregion

        #region HideReach()
        /// <summary>
        /// Hides currently shown reach, clears HighlitedFields list.
        /// </summary>
        public void HideReach()
        {
            foreach (var hex in _highlightedFields)
            {
                hex.hexObject.GetComponent<HexController>().RemoveCurrentSprite();
            }
            _highlightedFields.Clear();
        }
        #endregion

        #region ShowPieceInfo()
        private void ShowPieceInfo(global::Assets.Scripts.Controllers.PieceController piece)
        {
            GuiManager.PieceStatsPanel.ShowCreatureInfo(piece.Creature);
        }
        #endregion

        #region HidePieceInfo()
        private void HidePieceInfo()
        {
            GuiManager.PieceStatsPanel.HideCreatureInfo();
        }
        #endregion

        #region LoadSelectedPieceDataToGui()
        internal void LoadSelectedPieceDataToGui()
        {
            GuiManager.PieceStatsPanel.ShowCreatureInfo(_selectedPiece.Creature);
        }
        #endregion

        #region OnNextTurnButtonClick()
        public void OnNextTurnButtonClick()
        {
            _commandManager.ClearCommandList();
            DeselectPiece();

            GameManager.Turn.CurrentPlayer =
                GameManager.Turn.CurrentPlayer == PlayerType.PlayerOne ? PlayerType.PlayerTwo : PlayerType.PlayerOne;
            GuiManager.UpdateGui();
        }
        #endregion

        #region OnUndoButtonClick()
        /// <summary>
        /// Actions executed after clicking Undo button. Undoes last command and deselects piece.
        /// </summary>
        public void OnUndoButtonClick()
        {
            _commandManager.UndoCommand();
            DeselectPiece();
        }
        #endregion

        #region OnRedoButtonClick()

        /// <summary>
        /// Actions executed after clicking Redo button. Redoes last command and deselects piece.
        /// </summary>
        public void OnRedoButtonClick()
        {
            _commandManager.RedoCommand();
            DeselectPiece();
        }
        #endregion
    }
}
