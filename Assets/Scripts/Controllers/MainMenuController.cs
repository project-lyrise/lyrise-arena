﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Controllers
{
    public class MainMenuController : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
        
        }

        // Update is called once per frame
        private void Update()
        {
        
        }

        public void OnButtonClick(string buttonName)
        {
            switch (buttonName)
            {
                case "Menu.PlayButton":
                    OnPlayButtonClicked();
                    break;

                case "Menu.ExitButton":
                    OnExitButtonClicked();
                    break;
            }
        }

        private void OnPlayButtonClicked()
        {
            Debug.Log("Play button clicked");
            SceneManager.LoadScene("Game");
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game"));
        }

        private void OnExitButtonClicked()
        {
            Application.Quit();
        }
    }
}
