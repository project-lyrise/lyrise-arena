﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Interfaces;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class CommandManager
    {
        private readonly List<ICommand> _commandList;
        private ICommand _currentCommand;

        public CommandManager()
        {
            _commandList = new List<ICommand>();
            _currentCommand = null;
        }

        #region ExecuteCommand()
        /// <summary>
        /// Executes command and adds it to internal command list.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        public void ExecuteCommand(ICommand command)
        {
            try
            {
                command.Execute();
                var commandIndex = -1;

                if (_commandList.Count != 0)
                {
                    commandIndex = _commandList.IndexOf(_currentCommand);
                }

                if (commandIndex == -1 || (commandIndex + 1 == _commandList.Count))
                {
                    _commandList.Add(command);
                }
                else
                {
                    _commandList[commandIndex + 1] = command;
                }

                _currentCommand = command;
            }
            catch (NullReferenceException ex)
            {
                Debug.Log(ex.Message);
            }
        }
        #endregion

        #region UndoCommand()
        /// <summary>
        /// Undoes last executed command/
        /// </summary>
        public void UndoCommand()
        {
            if (_currentCommand == null) return;

            _currentCommand.Undo();
            var commandIndex = _commandList.IndexOf(_currentCommand);

            _currentCommand = commandIndex != 0 ? _commandList[commandIndex - 1] : null;
        }
        #endregion

        #region RedoCommand()
        /// <summary>
        /// Redoes last undo-ed command.
        /// </summary>
        public void RedoCommand()
        {
            if (_currentCommand == null && _commandList.Count > 0)
            {
                _currentCommand = _commandList[0];
                _currentCommand.Execute();
            }
            else if (_currentCommand != null && _commandList.Count > _commandList.IndexOf(_currentCommand) + 1)
            {
                _currentCommand = _commandList[_commandList.IndexOf(_currentCommand) + 1];
                _currentCommand.Execute();
            }
        }
        #endregion

        #region ClearCommandList()

        public void ClearCommandList()
        {
            _commandList.Clear();
        }
        #endregion
    }
}
