﻿using Assets.Scripts.Data;
using Assets.Scripts.Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Managers
{
    public static class GuiManager
    {
        private static readonly GuiText CurrentPlayerText = new GuiText(GameObject.Find("CurrentPlayerText").GetComponent<Text>());

        public static void InitializeGui()
        {
            PieceStatsPanel.Init();
            PieceDrawer.Init();
            CurrentPlayerText.SetTextVariable(GameManager.Turn.CurrentPlayer.ToString());
        }

        public static void UpdateGui()
        {
            CurrentPlayerText.SetTextVariable(GameManager.Turn.CurrentPlayer.ToString());
        }

        public static class PieceStatsPanel
        {
            private static readonly GameObject MainPanel = GameObject.Find("MainPanel");
            private static readonly GameObject NameHeader = GameObject.Find("NameHeader");

            private static readonly GuiText NameText = new GuiText(GameObject.Find("NameText").GetComponent<Text>());
            private static readonly GuiText HealthPointsText = new GuiText(GameObject.Find("HPText").GetComponent<Text>());
            private static readonly GuiText ActionPointsText = new GuiText(GameObject.Find("APText").GetComponent<Text>());
            private static readonly GuiText OffensivePowerText = new GuiText(GameObject.Find("OPText").GetComponent<Text>());
            private static readonly GuiText DefensivePowerText = new GuiText(GameObject.Find("DPText").GetComponent<Text>());

            public static void Init()
            {
                MainPanel.SetActive(false);
            }

            public static void ShowCreatureInfo(Creature creature)
            {
                LoadCreatureStats(creature);
                MainPanel.SetActive(true);
            }

            public static void HideCreatureInfo()
            {
                MainPanel.SetActive(false);
            }

            private static void LoadCreatureStats(Creature creature)
            {
                NameText.SetText(creature.Name);
                HealthPointsText.SetTextVariable(creature.HealthPoints.ToString());
                ActionPointsText.SetTextVariable(creature.ActionPoints.ToString());
                OffensivePowerText.SetTextVariable(creature.OffensivePower.ToString());
                DefensivePowerText.SetTextVariable(creature.DefensivePower.ToString());
            }
        }

        public static class PieceDrawer
        {
            private static readonly GameObject PieceDrawerPanel = GameObject.Find("PieceDrawerPanel");
            private static readonly Text PieceDrawerButtonText = GameObject.Find("PieceDrawerButtonText").GetComponent<Text>();

            public static void Init()
            {
                SetPieceDrawer(false);
            }

            public static void SetPieceDrawer(bool isActive)
            {
                PieceDrawerButtonText.text = (isActive ? "Hide " : "Show ") + "Piece Drawer"; 
                PieceDrawerPanel.SetActive(isActive);
            }

            public static void TogglePieceDrawer()
            {
                SetPieceDrawer(!PieceDrawerPanel.activeSelf);
            }
        }
    }
}
