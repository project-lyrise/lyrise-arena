﻿using UnityEngine;

namespace Assets.Scripts.Managers
{
    public static class PrefabManager
    {
        public static GameObject HexPrefab;
        public static GameObject PiecePrefab;
    }
}
