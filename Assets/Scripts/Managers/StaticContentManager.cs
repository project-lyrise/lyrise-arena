﻿namespace Assets.Scripts.Managers
{
    public static class StaticContentManager
    {
        public static class CreatureStats
        {
            public static readonly string HealthPoints = "Health Points: {0}";
            public static readonly string ActionPoints = "Action Points: {0}";
            public static readonly string OffensivePower = "Offensive Power: {0}";
            public static readonly string DefensivePower = "Defensive Power: {0}";
        }
    }
}
