﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Enums;
using UnityEngine;

namespace Assets.Scripts.Data
{
    public class Creature
    {
        public CreatureTemplate Template { get; }
        public string Name { get; set; }
        public int HealthPoints { get; set; }
        private int _actionPoints;
        public int ActionPoints
        {
            get => _actionPoints;

            set
            {
                if (value < 0)
                {
                    _actionPoints = 0;
                }
                else if (value > GetMaxActionPoints())
                {
                    _actionPoints = GetMaxActionPoints();
                }
                else
                {
                    _actionPoints = value;
                }
            }
        }
        public int OffensivePower { get; set; }
        public int DefensivePower { get; set; }
        public Dictionary<Aspect, float> AspectAffinity { get; set; }

        private Creature()
        {
            InitializeAspectAffinity();
        }

        public Creature(CreatureTemplate template) : this()
        {
            Template = template;
            HealthPoints = template.HealthPoints;
            ActionPoints = template.ActionPoints;
            OffensivePower = template.OffensivePower;
            DefensivePower = template.DefensivePower;
            AspectAffinity = template.AspectAffinity;
        }

        public Creature(CreatureTemplate template, string name) : this(template)
        {
            Name = name;
        }

        public int GetMaxActionPoints()
        {
            return Template.ActionPoints;
        }

        public Aspect GetDominatingAspect()
        {
            var strongestElementalAffinity = Mathf.Max(AspectAffinity.Values.ToArray());
            return AspectAffinity.FirstOrDefault(e => e.Value == strongestElementalAffinity).Key;
        }

        private void InitializeAspectAffinity()
        {
            AspectAffinity = new Dictionary<Aspect, float>
            {
                {Aspect.Fire, 0.2f},
                {Aspect.Earth, 0.2f},
                {Aspect.Light, 0.2f},
                {Aspect.Wind, 0.2f},
                {Aspect.Life, 0.2f},
                {Aspect.Shadow, 0.2f},
                {Aspect.Void, 0.2f},
                {Aspect.Water, 0.2f}
            };
        }
    }
}
