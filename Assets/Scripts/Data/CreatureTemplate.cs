﻿using System.Collections.Generic;
using Assets.Scripts.Enums;

namespace Assets.Scripts.Data
{
    public class CreatureTemplate
    {
        public string SpeciesName { get; set; }
        public int HealthPoints { get; set; }
        public int ActionPoints { get; set; }
        public int OffensivePower { get; set; }
        public int DefensivePower { get; set; }
        public Dictionary<Aspect, float> AspectAffinity { get; set; }
    }
}
