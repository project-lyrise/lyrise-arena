﻿using Assets.Scripts.Enums;

namespace Assets.Scripts.Data
{
    public class Orb
    {
        public int Level { get; set; }
        public Aspect Aspect { get; set; }
    }
}
