﻿namespace Assets.Scripts.Interfaces
{
    public interface ICommand 
    {
        void Execute();
        void Undo();
    }
}
