﻿using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public interface IShape
    {
        int Column { get; }
        int Row { get; }
    float Radius { get; }
        Vector3 Position { get; }
        void SetPosition(Vector2 coords);
    }
}
