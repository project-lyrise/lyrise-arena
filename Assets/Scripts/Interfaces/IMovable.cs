﻿namespace Assets.Scripts.Interfaces
{
    public interface IMovable
    {
        IShape CurrentField { get; }
        void Move(IShape destination, bool isUndoing = false);
    }
}
