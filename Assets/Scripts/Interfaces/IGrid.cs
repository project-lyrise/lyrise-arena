﻿using System.Collections.Generic;
using Assets.Scripts.Controllers;

namespace Assets.Scripts.Interfaces
{
    public interface IGrid
    {
        List<IShape> FieldList { get; set; }

        void GenerateGrid();
        void ShowReach(PieceController piece, IShape field);
        void HideReach();
    }
}
