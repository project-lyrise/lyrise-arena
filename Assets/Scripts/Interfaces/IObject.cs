﻿using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public interface IObject
    {
        Vector3 Position { get; }
        IShape CurrentField { get; set; }
        GameObject ObjectPrefab { get; set; }
    }
}
