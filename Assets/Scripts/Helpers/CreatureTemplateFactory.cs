﻿using System.Collections.Generic;
using Assets.Scripts.Data;
using Assets.Scripts.Enums;

namespace Assets.Scripts.Helpers
{
    public static class CreatureTemplateFactory
    {
        public enum Species
        {
            TestSpecies
        }

        public static CreatureTemplate CreateTemplate(Species creatureSpecies)
        {
            switch(creatureSpecies)
            {
                case Species.TestSpecies:
                    return new CreatureTemplate()
                    {
                        SpeciesName = "TestSpecies",
                        HealthPoints = 100,
                        ActionPoints = 2,
                        OffensivePower = 12,
                        DefensivePower = 20,
                        AspectAffinity = new Dictionary<Aspect, float>
                        {
                            { Aspect.Fire, 0.8f },
                            { Aspect.Earth, 0.5f },
                            { Aspect.Light, 0.3f },
                            { Aspect.Wind, 0.2f },
                            { Aspect.Life, 0.2f },
                            { Aspect.Shadow, 0.2f },
                            { Aspect.Void, 0.2f },
                            { Aspect.Water, 0.2f }
                        }
                    };

                default:
                    return new CreatureTemplate()
                    {
                        SpeciesName = "TestSpecies",
                        HealthPoints = 100,
                        ActionPoints = 2,
                        OffensivePower = 12,
                        DefensivePower = 20,
                        AspectAffinity = new Dictionary<Aspect, float>
                        {
                            { Aspect.Fire, 0.8f },
                            { Aspect.Earth, 0.5f },
                            { Aspect.Light, 0.3f },
                            { Aspect.Wind, 0.2f },
                            { Aspect.Life, 0.2f },
                            { Aspect.Shadow, 0.2f },
                            { Aspect.Void, 0.2f },
                            { Aspect.Water, 0.2f }
                        }
                    };
            }
        }
    }
}
