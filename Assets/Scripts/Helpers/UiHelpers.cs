using UnityEngine.UI;

namespace Assets.Scripts.Helpers
{
    public class GuiText
    {
        private string _template { get; set; }
        public Text Text { get; set; }

        public GuiText(Text text)
        {
            _template = text.text;
            Text = text;
        }

        public void SetText(string text)
        {
            _template = text;
            Text.text = text;
        }

        public void SetTextVariable(string textVariable)
        {
            Text.text = string.Format(_template, textVariable);
        }
    }
}