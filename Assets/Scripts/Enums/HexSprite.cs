namespace Assets.Scripts.Enums
{
    public enum HexSprite
    {
        Default,
        Hover,
        Movable,
        Ally,
        Enemy
    }
}