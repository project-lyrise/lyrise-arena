﻿namespace Assets.Scripts.Enums
{
    public enum Aspect
    {
        Life = 1,
        Fire = 2,
        Light = 3,
        Wind = 4,
        Void = 5,
        Water = 6,
        Shadow = 7,
        Earth = 8
    }
}
