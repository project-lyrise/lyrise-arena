﻿using System.Collections.Generic;
using Controllers;
using Data;
using Enums;
using Helpers;
using Managers;
using UnityEngine;
using static Helpers.CreatureTemplateFactory;

public class _AppStart : MonoBehaviour
{
    public GameObject HexPrefab;
    public GameObject PiecePrefab;

    private void Awake()
    {
        InitializeManagers();
        InitializePieces();
    }

    private void InitializePieces()
    {
        GameObject gameGrid = GameObject.Find("GameGrid");

        Creature creatureOne = new Creature(CreateTemplate(Species.TestSpecies));

        Creature creatureTwo = new Creature(CreateTemplate(Species.TestSpecies));

        var pieceOne = Instantiate(PrefabManager.PiecePrefab, new Vector3(), new Quaternion());
        var pieceTwo = Instantiate(PrefabManager.PiecePrefab, new Vector3(), new Quaternion());
        pieceOne.transform.parent = gameGrid.transform;
        pieceTwo.transform.parent = gameGrid.transform;

        pieceOne
            .GetComponent<PieceController>()
            .InitializeCreatureData(creatureOne, new Hex(1, 1, 0.5F), PlayerType.PlayerOne);

        pieceTwo
            .GetComponent<PieceController>()
            .InitializeCreatureData(creatureTwo, new Hex(-1, 1, 0.5F), PlayerType.PlayerTwo);
    }

    private void InitializeManagers()
    {
        //Here you can initialize all scene managers

        /* Prefab Manager */
        PrefabManager.HexPrefab = HexPrefab;
        PrefabManager.PiecePrefab = PiecePrefab;
        
        /* Gui Manager */
        GuiManager.InitializeGui();
    }
}