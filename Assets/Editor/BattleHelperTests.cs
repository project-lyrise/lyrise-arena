﻿using System.Collections.Generic;
using Assets.Scripts.Data;
using Assets.Scripts.Enums;
using Assets.Scripts.Helpers;
using NUnit.Framework;

namespace Assets.Editor
{
    public class BattleHelperTests
    {
        private Skill offensiveSkill;
        private Skill defensiveSkill;
        private Creature testCreature;

        [SetUp]
        public void Init()
        {
            offensiveSkill = new Skill
            {
                APCost = 5,
                Power = 10,
                SuccessChance = 0.7f,
                SkillType = SkillType.Offensive,
                SkillTarget = SkillTarget.Creature,
                AttachedOrbs = new List<Orb>
                {
                    new Orb {Aspect = Aspect.Fire, Level = 1},
                    new Orb {Aspect = Aspect.Earth, Level = 1}
                }
            };

            defensiveSkill = new Skill
            {
                APCost = 3,
                Power = 12,
                SuccessChance = 0.9f,
                SkillType = SkillType.Defensive,
                SkillTarget = SkillTarget.Caster,
                AttachedOrbs = new List<Orb>
                {
                    new Orb {Aspect = Aspect.Life, Level = 1},
                    new Orb {Aspect = Aspect.Light, Level = 1}
                }
            };

            testCreature =
                new Creature(CreatureTemplateFactory.CreateTemplate(CreatureTemplateFactory.Species.TestSpecies));
        }

        [Test]
        public void CalculateSkillPower_OffensiveSkill_Test()
        {
            // Arrange
            Creature creature = testCreature;
            Skill skill = offensiveSkill;

            // Act
            int result = BattleHelper.CalculateSkillPower(skill, creature);

            // Assert
            Assert.AreEqual(result, 18);
        }

        [Test]
        public void CalculateSkillPower_DefensiveSkill_Test()
        {
            // Arrange
            Creature creature = testCreature;
            Skill skill = defensiveSkill;

            // Act
            int result = BattleHelper.CalculateSkillPower(skill, creature);

            // Assert
            Assert.AreEqual(result, 6);
        }
    }
}
